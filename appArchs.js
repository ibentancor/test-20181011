var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');

var app = express();
var port = process.env.PORT || 3000;
const URI = '/api-uruguay/v1/';
var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
var diasSemana = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");
var hoy = new Date();
var userFile = require('./users.json');
var ctaFile = require('./cuentas.json');
app.use(bodyParser.json());



app.listen(port);
console.log('Escuchando en el puerto -->' + port + '<--');


//GET saludo
app.get(URI,
  function(req, res) {
    res.send('Hola API desde Node, hoy es ' + hoy.getDate() + '/' + meses[hoy.getMonth()] + '/' + hoy.getFullYear());
  }
);

//GET users
app.get(URI + 'users',
  function(req, res) {
    res.sendFile('users.json', {
      root: __dirname
    });
    //    console.log(users.json);
  }
);

//GET users
app.get(URI + 'cuentas',
  function(req, res) {
    res.sendFile('cuentas.json', {
      root: __dirname
    });
    //    console.log(users.json);
  }
);

//GET user con ID
app.get(URI + 'users/:idUser',
  function(req, res) {
    let idUser = req.params.idUser;
    res.send(userFile[idUser - 1]);
    //    console.log(users.json);
  }
);

//GET cuenta con ID
app.get(URI + 'cuentas/:idCta',
  function(req, res) {
    let idCta = req.params.idCta;
    res.send(ctaFile[idCta - 1]);
    //    console.log(users.json);
  }
);

//POST users con bodyParser
app.post(URI + 'users',
  function(req, res) {
    console.log('POST usersen Body');
    console.log(req.body);
    //    var msg = {'message':'POST realizado correctamente'};
    var tamanio = userFile.length + 1;
    var userNuevo = {
      'id': tamanio,
      'first_name': req.body.first_name,
      'last_name': req.body.last_name,
      'email': req.body.email,
      'password': req.body.password
    };
    userFile.push(userNuevo);

    res.send({
      'msg': 'Nuevo usuario añadido correctamente',
      userNuevo
    });
  });

//POST users con headers
app.post(URI + 'users2',
  function(req, res) {
    console.log('POST users en Header');
    console.log(req.headers);
    //    var msg = {'message':'POST realizado correctamente'};
    var tamanio = userFile.length + 1;
    var userNuevo = {
      'id': tamanio,
      'first_name': req.headers.first_name,
      'last_name': req.headers.last_name,
      'email': req.headers.email,
      'password': req.headers.password
    };
    userFile.push(userNuevo);

    res.send({
      'msg': 'Nuevo usuario añadido correctamente',
      userNuevo
    });
  });

//PUT users
app.put(URI + 'users/:id',
  function(req, res) {
    console.log('PUT users');
    var idUpdate = req.params.id;
    var existe = true;
    var userNuevo;
    if (userFile[idUpdate - 1] == undefined) {
      console.log("Usuario NO existe");
      existe = false;
    } else {
      console.log("Usuario existe");
      userNuevo = {
        'id': idUpdate,
        'first_name': req.body.first_name,
        'last_name': req.body.last_name,
        'email': req.body.email,
        'password': req.body.password
      }
      userFile[idUpdate - 1, 1] = userNuevo;
    }
    var msgResponde = existe ? {
      'msg': 'Update OK', userNuevo
    } : {
      'msg': 'Update failed'
    };
    //      msgResponde += userNuevo;
    var statusCode = existe ? 200 : 400;
    res.status(statusCode).send(msgResponde);
    //      res.status(statusCode);
  });


//DELETE users
app.delete(URI + 'users/:id',
  function(req, res) {
    console.log('DELETE users');
    var idUpdate = req.params.id;
    var existe = true;
    var userNuevo;
    if (userFile[idUpdate - 1] == undefined) {
      console.log("Usuario NO existe");
      existe = false;
    } else {
      console.log("Usuario existe");
      userFile.splice(idUpdate - 1, 1);
    }
    var msgResponde = existe ? {
      'msg': 'Delete OK'
    } : {
      'msg': 'Delete failed'
    };
    //      msgResponde += userNuevo;
    var statusCode = existe ? 200 : 400;
    res.status(statusCode).send(msgResponde);
    //      res.status(statusCode);
  });

//GET con query string
app.get(URI + "usersQ",
  function(req, res) {
    console.log('GET con query string');
    let msgResponde = {'msg':'Usuario NO encontrado'};
    let contador = 0;
    let nom = req.query.qfname + " " + req.query.qlname;
    for (var user in userFile){
      console.log(userFile[contador].first_name, user, userFile[contador].last_name, req.query.qfname, req.query.qlname);
      if (userFile[user].first_name == req.query.qfname && userFile[user].last_name == req.query.qlname){
        msgResponde = {'msg':'Usuario encontrado ->', user};
      }
      contador += 1;
    }
//    msgResponde = {msgResponde, contador, nom};
    res.send(msgResponde);
//    res.send(contador);

//    userFile.filter();
  });
